# Webclowder

### WARNING: please do not use the software if doing so violates terms of service, the law, or people's personal space.
If you do something silly, that's your own damn fault. For more details see the LICENSE file.

## What is?

A group of cats is called a clowder. A group of webcats is called a webclowder.

## What for?

Webclowder is a tool designed for schools or other educational institutions to test parts of their library systems in an automated fashion.

## How set up?

1. Have python3 with pipenv installed.
2. Clone this project.
3. `pipenv install`
4. Fill in the values at the top of the page
5. `pipenv run python pounce.py`

## How automate?

Repeat step 5 at regular intervals. A usual prescription is every 5-7 days.

## It broke!

This software may be maintained when I have time. If you manage to fix it yourself, please submit a pull request.

## This is horrific code!

Yes. This is a script written in about half an hour to solve an annoying problem. This is not good code.
